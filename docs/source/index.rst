.. colindex2 documentation master file, created by
   sphinx-quickstart on Mon Sep  9 17:47:12 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to colindex2's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   colindex2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
