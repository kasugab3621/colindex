colindex2 package
=================

colindex2.colindex2 module
--------------------------

.. automodule:: colindex2.colindex2
   :members:
   :undoc-members:
   :show-inheritance:

colindex2.tracking\_overlap2 module
-----------------------------------

.. automodule:: colindex2.tracking_overlap2
   :members:
   :undoc-members:
   :show-inheritance:

colindex2.draw\_map module
--------------------------

.. automodule:: colindex2.draw_map
   :members:
   :undoc-members:
   :show-inheritance:

colindex2.get\_IDfile module
----------------------------

.. automodule:: colindex2.get_IDfile
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: colindex2
   :members:
   :undoc-members:
   :show-inheritance:
