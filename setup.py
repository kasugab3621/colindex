import setuptools

with open("./colindex2/README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="colindex2",
    version="v2.7.8",
    author="Satoru Kasuga",
    author_email="kasugab3621@outlook.com",
    description="depression detection/tracking schemes",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kasugab3621/colindex2.git",
    packages=setuptools.find_packages(),
    install_requires=['numpy', 'pandas', 'xarray', 'numba', 'netCDF4', 'h5netcdf'],
    entry_points={
        'console_scripts': [
            'gen_data_settings=colindex2.commands:gen_data_settings',
            'detect=colindex2.commands:detect',
            'track=colindex2.commands:track',
            'find_track=colindex2.commands:find_track',
            'draw_map=colindex2.draw_map:draw_map',
            ]}
)
