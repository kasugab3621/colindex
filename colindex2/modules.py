from numba import jit, njit
import numba
import numpy as np
import numpy.linalg as LA
import xarray as xr


def conv_second(dt):
    # 'H' and 'T' are deprecated since pandas-2.2.0
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases

    if 'H' in dt or 'h' in dt:
        if 'H' == dt:
            _h = 1
        else:
            _h = int(dt[:-1])
        return 60*60*_h

    elif 'D' in dt:
        if 'D' == dt:
            _d = 1
        else:
            _d = int(dt[:-1])
        return 60*60*24*_d

    elif 'T' in dt or 'min' in dt:
        if 'T' == dt:
            _T = 1
        else:
            _T = int(dt[:-1])
        return 60*_T


def great_circle_distance(_lon1, _lat1, _lon2, _lat2, r=6371.):
    """calculate great-circle distance [km]"""

    if type(_lon1) != type(np.array([0])) and type(_lon2) != type(np.array([0])):
        arrays = False
        if _lon1 == _lon2 and _lat1 == _lat2:
            return 0.
    else:
        arrays = True

    lon1 = _lon1*np.pi/180
    lon2 = _lon2*np.pi/180
    lat1 = _lat1*np.pi/180
    lat2 = _lat2*np.pi/180

    if not arrays:
        ds = np.arccos(np.sin(lat1)*np.sin(lat2)
                       +np.cos(lat1)*np.cos(lat2)*np.cos(abs(lon1-lon2)))
    else:
        lon_bool = np.where(lon1==lon2, True, False)
        lat_bool = np.where(lat1==lat2, True, False)
        ds = np.where((lon_bool)&(lat_bool), 0.,
                      np.arccos(np.sin(lat1)*np.sin(lat2)
                                +np.cos(lat1)*np.cos(lat2)*np.cos(abs(lon1-lon2))))

    return r*ds


@njit
def great_circle_distance_numba(_lon1, _lat1, _lon2, _lat2) :
    """calculate great-circle distance [km]"""

    rr = 6371.

    if _lon1 == _lon2 and _lat1 == _lat2:
        return 0.

    lon1 = _lon1*np.pi/180
    lon2 = _lon2*np.pi/180
    lat1 = _lat1*np.pi/180
    lat2 = _lat2*np.pi/180

    ds = np.arccos(np.sin(lat1)*np.sin(lat2)
                   +np.cos(lat1)*np.cos(lat2)*np.cos(abs(lon1-lon2)))

    return np.float32(rr*ds)


@njit
def moving_direction(lon_B, lat_B, lon_F, lat_F):
    
    b = great_circle_distance_numba(lon_B, lat_B, lon_F, lat_F)
    a = great_circle_distance_numba(lon_F, lat_B, lon_F, lat_F)

    alpha = np.arcsin(a/b)  # returns 0 ~ pi/2 since 0 < a/b < 1
    # shogen (temae/oku -> east/west x north/south)
    d_lon_abs = np.abs(lon_B - lon_F)
    d_lon = lon_F - lon_B
    d_lat1 = lat_F - lat_B
    d_lat2 = lat_F + lat_B
    
    temae = d_lon_abs < 90. or 270. <= d_lon_abs < 360.
    oku = not temae
    east = 0. < d_lon < 180. or -360 < d_lon < -180.
    west = not east 
    
    shogen = 0
    
    if temae and east:
        if d_lat1 > 0: shogen = 1
        else: shogen = 4
    if temae and west:
        if d_lat1 > 0: shogen = 2
        else: shogen = 3
    if oku and east:
        if d_lat2 > 0: shogen = 1
        else: shogen = 4
    if oku and west:
        if d_lat2 > 0: shogen = 2
        else: shogen = 3                    

    if shogen == 0:
        raise ValueError('shogen error!')
        
    if shogen == 1:
        azimuth = alpha
    elif shogen == 2:
        azimuth = np.pi - alpha
    elif shogen == 3:
        azimuth = np.pi + alpha
    elif shogen == 4:
        azimuth = 2*np.pi - alpha

    return np.float32(azimuth)


@njit
def invert_gcd2_array(lons, lats, theta, d):

    lons_flat = lons.flatten()
    lats_flat = lats.flatten()
    org_shape = lons.shape
    lon_end_flat = np.empty_like(lons_flat)
    lat_end_flat = np.empty_like(lats_flat)

    for i in range(len(lons_flat)):
        lon = lons_flat[i]
        lat = lats_flat[i]
        _ret = invert_gcd2(lon, lat, theta, d)
        lon_end_flat[i] = _ret[0]
        lat_end_flat[i] = _ret[1]
    lons_end = lon_end_flat.reshape(org_shape)
    lats_end = lat_end_flat.reshape(org_shape)

    return lons_end, lats_end


@njit
def invert_gcd1_array(lons, lats, theta, d):

    lons_flat = lons.flatten()
    lats_flat = lats.flatten()
    org_shape = lons.shape
    lon_end_flat = np.empty_like(lons_flat)
    lat_end_flat = np.empty_like(lats_flat)

    for i in range(len(lons_flat)):
        lon = lons_flat[i]
        lat = lats_flat[i]
        _ret = invert_gcd1(lon, lat, theta, d)
        lon_end_flat[i] = _ret[0]
        lat_end_flat[i] = _ret[1]
    lons_end = lon_end_flat.reshape(org_shape)
    lats_end = lat_end_flat.reshape(org_shape)

    return lons_end, lats_end


@njit
def invert_gcd2(_lon, _lat, _theta, _d):
    """
    invert great circle distance and get new coordinate
    using Spherical Trigonometry

    Parameters
    ----------
    lon, lat: coordinate of the start point [degree]
    theta: angle from east toward the end point [degree]
    d: travel distance [km]

    Return
    ------
    coordinate(s) of the end point(s) (lon_end, lat_end) [degree]
    """

    # change 32 -> 64
    lon = np.float64(_lon)
    lat = np.float64(_lat)
    theta = np.float64(_theta)
    d = np.float64(_d)

    #rr = np.float32(6371.)  # earth radius [km]
    rr = 6371.

    # convert angles within +-360
    theta2 = np.divmod(theta, 360.)[1]

    A = np.deg2rad(90.-theta2)
    c = d/rr  # center angle between pos. vectors of start and end [radian]

    b = np.deg2rad(90.-lat)  # zenith of start point

    # cosine formula of Spherical Trigonometry
    cos_a = np.cos(b)*np.cos(c) + np.sin(b)*np.sin(c)*np.cos(A)
    a = np.arccos(cos_a)  # zenith of end point [radian]
    lat_end = 90. - np.rad2deg(a)

    if np.sin(b) == 0.:  #start point is N/S pole
        return theta2, lat_end
    if np.sin(a) == 0.:  #end point is N/S pole
        return 0., lat_end

    _N = np.cos(c) - np.cos(a)*np.cos(b)
    _D = np.sin(a)*np.sin(b)
    cos_C = _N/_D

    if cos_C > 1.:
        C = 0.
    elif cos_C < -1.:
        C = np.pi
    else:
        C = np.arccos(cos_C)  # memo: np.arccos() retruns positive value
    
    if -90 < theta2 < 90. or 270. < theta2:
        lon_end = lon+np.rad2deg(C)
    else:
        lon_end = lon-np.rad2deg(C)

    # convert angles within +-360
    lon_end2 = np.divmod(lon_end, 360.)[1]

    # convert 64 -> 32 and return
    return np.float32(lon_end2), np.float32(lat_end)



@njit
def invert_gcd1(_lon, _lat, _theta, _d):
    # change 32 -> 64
    lon = np.float64(_lon)
    lat = np.float64(_lat)
    theta = np.float64(_theta)
    d = np.float64(_d)

    rr = 6371.  # earth radius [km]

    # cannot calc polar region
    if lat < -75 or lat > 75:
        return np.float32(np.nan), np.float32(np.nan)

    b = np.deg2rad(90.-lat)  # zenith of start point

    if theta == 0.:
        lat_end2 = lat
        c = d/(rr*np.cos(np.deg2rad(lat)))
        lon_end2 = lon + np.rad2deg(c)

    if theta == 90.:
        lon_end = lon
        c = d/rr
        lat_end = lat + np.rad2deg(c)

        # convert angles within +-90 in lat
        if lat_end > 90.:
            lon_end2 = 180.+lon_end
            lat_end2 = 180.-lat_end
        else:
            lon_end2 = lon_end
            lat_end2 = lat_end

    if theta == 180.:
        lat_end2 = lat
        c = d/(rr*np.cos(np.deg2rad(lat)))
        lon_end2 = lon - np.rad2deg(c)

    if theta == 270.:
        lon_end = lon
        c = d/rr
        lat_end = lat - np.rad2deg(c)

        # convert angles within +-90 in lat
        if lat_end < -90.:
            lon_end2 = 180.+lon_end
            lat_end2 = -180.-lat_end
        else:
            lon_end2 = lon_end
            lat_end2 = lat_end

    # convert angles within +-360 in lon, +-90 in lat
    lon_end3 = np.divmod(lon_end2, 360.)[1]

    return np.float32(lon_end3), np.float32(lat_end2)


@njit
def check_zonal_loop(lons):

    dlon = lons[1] - lons[0]

    if lons[-1] == lons[0]:
        return 'loop1'  # already overlapped

    l = lons[-1] + dlon

    if l == lons[0] or l+360 == lons[0] or l-360 == lons[0]: 
        return 'loop0'  # not overlapped
    else:
        return 'region'


def get_center_field(da, lon, da_bool=False):
    # converting lon2 -> lon1
    if da_bool:
        c = da.sel(longitude=lon)
    else:
        c = da.sel(longitude=lon).data
    return c


@njit
def interp_numba(ar, lons, lats, lons3, lats3):
    
    iy, ix = lons3.shape
    array = np.full((iy, ix), np.nan, dtype='f4')

    for j in range(iy):
        _lat = lats3[j]

        for i in range(ix):
            _lon = lons3[j, i]

            if np.isnan(_lon) or np.isnan(_lat): continue

            if _lon < lons.min() or _lon > lons.max():
                array[j, i] = np.nan
                continue
            if _lat < lats.min() or _lat > lats.max():
                array[j, i] = np.nan
                continue

            # search nearest
            dlat = 100000.
            nearj = -1
            for jj, rlat in enumerate(lats):
                _dlat = np.abs(_lat-rlat)
                if _dlat < dlat:
                    dlat = _dlat
                    nearjj = nearj  # second nearest
                    nearj = jj  # nearest
            if nearjj == -1:  nearjj = 1

            dlon = 100000.
            neari = -1
            for ii, rlon in enumerate(lons):
                _dlon = np.abs(_lon-rlon)
                if _dlon < dlon:
                    dlon = _dlon
                    nearii = neari  # second nearest
                    neari = ii  # nearest
            if nearii == -1:  nearii = 1
                
            #  A---|------D
            #  |b        b|
            #  P---X------Q
            #  | c    d   |
            #  |          |
            #  |a        a|
            #  B---|------C

            a = np.abs(_lat-lats[nearjj])
            b = np.abs(_lat-lats[nearj])
            c = np.abs(_lon-lons[neari])
            d = np.abs(_lon-lons[nearii])

            A = ar[nearj, neari]
            B = ar[nearjj, neari]
            C = ar[nearjj, nearii]
            D = ar[nearj, nearii]
            
            array[j, i] = ((A*a+B*b)*d+(D*a+C*b)*c)/(a+b)/(c+d)

    return array


def add_cyclic_point_hand(da):
    lon = da.longitude.values
    last_lon = lon[-1] + (lon[1]-lon[0])
    da0 =  da.isel(longitude=0)
    da0['longitude'] = last_lon
    return xr.concat([da, da0], dim='longitude')


def add_cyclic_point_hand2(ar, lon):
    """add index 0 after index -1
    01234 -> 012340 """
    last_lon = lon[-1] + (lon[1]-lon[0])
    #last_ar =  ar[:, 0][:, np.newaxis]
    last_ar =  ar[...,:, 0][...,:, np.newaxis]
    ar = np.concatenate([ar, last_ar], axis=-1)
    lon = np.concatenate([lon, np.array([last_lon])])
    return ar, lon


@njit
def add_cyclic_point_hand3(ar):
    """add index 0 after index -1 and add -1 before 0
    01234 -> 4012340 """
    s2 = (ar.shape[0], ar.shape[1], ar.shape[2]+2)
    ar2 = np.empty(s2, dtype='f4')
    ar2[...,0] = ar[..., -1]
    ar2[...,1:-1] = ar
    ar2[...,-1] = ar[..., 0]
    return ar2


@njit
def _allocate_envelope(AS3, r, m3, n3, idxp, idxn):
    
    ir, iy, ix = AS3.shape
    AS_en = np.zeros((2, iy, ix), dtype='f4')
    m_en = np.zeros((2, iy, ix), dtype='f4')
    n_en = np.zeros((2, iy, ix), dtype='f4')
    r_en = np.zeros((2, iy, ix), dtype='f4')

    for j in range(iy):
        for i in range(ix):

            AS_en[0, j, i] = AS3[idxp[j,i], j, i]
            m_en[0, j, i] = m3[idxp[j,i], j, i]
            n_en[0, j, i] = n3[idxp[j,i], j, i]
            r_en[0, j, i] = r[idxp[j,i]]

            AS_en[1, j, i] = AS3[idxn[j,i], j, i]
            m_en[1, j, i] = m3[idxn[j,i], j, i]
            n_en[1, j, i] = n3[idxn[j,i], j, i]
            r_en[1, j, i] = r[idxn[j,i]]

    return AS_en, r_en, m_en, n_en


@njit
def _envelope_AS_numba(c, lon, lat, r, mesh, da_bg, dblons, dblats, stencil):

    iy, ix = len(lat), len(lon)  # may be skipped, not looped
    ir = len(r)

    AS3 = np.empty((ir, iy, ix), dtype='f4')  # not looped
    m3 = np.empty((ir, iy, ix), dtype='f4')
    n3 = np.empty((ir, iy, ix), dtype='f4')
    around = np.empty((ir,8,iy,ix), dtype='f4')

    lons2, lats2, ths = mesh

    for _r in range(ir):

        if stencil == '9g':
            for h, _ in enumerate(ths):
                around[_r,h] = interp_numba(da_bg, dblons, dblats,
                                         lons2[_r, h, :, :], lats2[_r, h, :, 0])
            e, ne, n, nw, w, sw, s, se = around[_r]

            AS3[_r] = (n+s+e+w+ne+nw+sw+se-np.float32(8.)*c) / np.float32(8.) / r[_r]
            m3[_r] = (e-w)/np.float32(4.)/r[_r] + (ne+se-nw-sw)/np.float32(8.)/r[_r]/np.cos(np.pi/np.float32(4.))
            n3[_r] = (n-s)/np.float32(4.)/r[_r] + (ne+nw-se-sw)/np.float32(8.)/r[_r]/np.cos(np.pi/np.float32(4.))

        elif stencil == '5g' or stencil == '5l':
            for h, _ in enumerate(ths):
                around[_r,h] = interp_numba(da_bg, dblons, dblats, 
                                         lons2[_r, h, :, :], lats2[_r, h, :, 0])
            e, n, w, s = around[_r,0], around[_r,1], around[_r,2], around[_r,3]

            AS3[_r] = (n+s+e+w-np.float32(4.)*c) / np.float32(4.) / r[_r]
            m3[_r] = (e-w) / np.float32(2.) / r[_r]
            n3[_r] = (n-s) / np.float32(2.) / r[_r]

        else:
            raise ValueError('wrong stencil, choose from "9g", "5g", "5l"')
    
    AS3_m = np.where(np.isnan(AS3), 0., AS3).astype('f4')
    idxp = np.argmax(AS3_m, axis=0)
    idxn = np.argmin(AS3_m, axis=0)

    AS_en, r_en, m_en, n_en = _allocate_envelope(AS3_m, r, m3, n3, idxp, idxn)

    return AS3_m, AS_en, r_en, m_en, n_en


@njit
def search_localextrema(f, maxmin='both', relax=2, zloop=False,
                        as2=np.array([[0]],dtype='f4')):
    iy, ix = f.shape

    if zloop:    # zonal loop
        wide = np.empty((iy, ix+2), dtype='f4')
        wide[:, :1] = f[:, -1:]
        wide[:, 1:-1] = f[:, :]
        wide[:, -1:] = f[:, :1]
        o = f[1:-1, :]  # center
    else:
        wide = f
        o = f[1:-1, 1:-1]  # center

    as2_flag = False
    if as2.shape != (1,1):
        as2_flag = True
        if zloop:
            ant = as2[1:-1,:]
        else:
            ant = as2[1:-1,1:-1]

    # around 8 grids
    # 8: maxima, -8: minima
    stencil_num = 8

    if maxmin == 'both' or 'max':
        lmpH = np.full((iy, ix), False)
        # max search
        n = np.where(wide[ 2:  , 1:-1] - o <= 0, 1, 0)
        s = np.where(wide[  :-2, 1:-1] - o <= 0, 1, 0)
        w = np.where(wide[ 1:-1,  :-2] - o <= 0, 1, 0)
        e = np.where(wide[ 1:-1, 2:  ] - o <= 0, 1, 0)
        ne = np.where(wide[ 2:  , 2:  ] - o <= 0, 1, 0)
        se = np.where(wide[  :-2, 2:  ] - o <= 0, 1, 0)
        nw = np.where(wide[ 2:  ,  :-2] - o <= 0, 1, 0)
        sw = np.where(wide[ :-2,  :-2] - o <= 0, 1, 0)
        if as2_flag:
            _S = n + s + w + e + ne + se + nw + sw
            S = np.where(np.abs(o)>np.abs(ant),_S,0)
        else:
            S = n + s + w + e + ne + se + nw + sw
        if zloop:
            lmpH[1:-1, :] = np.where(S>=stencil_num-relax, True, False)
        else:
            lmpH[1:-1, 1:-1] = np.where(S>=stencil_num-relax, True, False)
        if maxmin == 'max':
            return lmpH

    if maxmin == 'both' or 'min':
        lmpL = np.full((iy, ix), False)
        # min search
        n2 = np.where(wide[ 2:  , 1:-1] - o >= 0, 1, 0)
        s2 = np.where(wide[  :-2, 1:-1] - o >= 0, 1, 0)
        w2 = np.where(wide[ 1:-1,  :-2] - o >= 0, 1, 0)
        e2 = np.where(wide[ 1:-1, 2:  ] - o >= 0, 1, 0)
        ne2 = np.where(wide[ 2:  , 2:  ] - o >= 0, 1, 0)
        se2 = np.where(wide[  :-2, 2:  ] - o >= 0, 1, 0)
        nw2 = np.where(wide[ 2:  ,  :-2] - o >= 0, 1, 0)
        sw2 = np.where(wide[ :-2,  :-2] - o >= 0, 1, 0)
        if as2_flag:
            _S2 = n2 + s2 + w2 + e2 + ne2 + se2 + nw2 + sw2
            S2 = np.where(np.abs(o)>np.abs(ant),_S2,0)
        else:
            S2 = n2 + s2 + w2 + e2 + ne2 + se2 + nw2 + sw2
        if zloop:
            lmpL[1:-1, :] = np.where(S2>=stencil_num-relax, True, False)
        else:
            lmpL[1:-1, 1:-1] = np.where(S2>=stencil_num-relax, True, False)
        if maxmin == 'min':
            return lmpL

    return lmpL + lmpH


@njit
def search_nearest2(coords, target):

    # check target is in coords
    if target < coords.min() or coords.max() < target:
        return 0, 0

    first = True
    for i in range(len(coords)):

        v = coords[i]
        d_new = np.abs(target-v)

        if first:
            first = False
            d = d_new
            ni = i
            continue

        if d_new < d:
            d = d_new
            ni = i

    if ni == 0:
        nii = ni + 1

    elif ni == len(coords)-1:
        nii = ni - 1

    elif np.abs(target-coords[ni+1]) < np.abs(target-coords[ni-1]):
        nii = ni + 1

    else:
        nii = ni - 1

    return ni, nii


@njit
def bilinear_interp(ar, lons, lats, _lon, _lat):         
    """bi-liner interpolation"""

    shape = ar.shape

    if len(lons) not in shape or len(lats) not in shape:
        raise ValueError('lons, lats, levs, and times must have the same coordinate in ar')

    ni, nii = search_nearest2(lons, _lon)
    nj, njj = search_nearest2(lats, _lat)

    a = np.abs(_lat-lats[njj])
    b = np.abs(_lat-lats[nj])
    c = np.abs(_lon-lons[ni])
    d = np.abs(_lon-lons[nii])

    A = ar[nj, ni]
    B = ar[njj, ni]
    C = ar[njj, nii]
    D = ar[nj, nii]

    return ((A*a+B*b)*d+(D*a+C*b)*c)/(a+b)/(c+d)


@njit
def _deg2km_lon(dlon, lat, rr):
    return np.pi*rr*np.cos(np.deg2rad(lat))*dlon*np.float32(2./360.)

@njit
def _deg2km_lat(dlat, rr):
    return np.pi*rr*dlat*np.float32(2./360.)

@njit
def _km2deg_lon(dx, lat, rr):
    return dx*np.float32(360./2.)/np.pi/rr/np.cos(np.deg2rad(lat))

@njit
def _km2deg_lat(dy, rr):
    return dy*np.float32(360./2.)/np.pi/rr

@njit
def _search_off_grid3(a, lon, lat, rad, lmp, rr, zloop, rad_ls):

    dlon = lon[1] - lon[0]  # deg
    dlat = lat[1] - lat[0]  # deg, can be negative
    dz = rad[1] - rad[0]  # km
    
    #r_ls, y_ls, x_ls = np.where(lmp)
    y_ls, x_ls = np.where(lmp)
    #print('original lmp', len(r_ls))

    # remove y boundary (can be north/south pole)
    #lmp[:, 0, :] = False
    #lmp[:, -1, :] = False
    lmp[0, :] = False
    lmp[-1, :] = False
    # remove x boundary for not zonal
    if lon.shape[0] == a.shape[-1]:
        zonal = False
        #lmp[:, :, 0] = False
        #lmp[:, :, -1] = False
        lmp[:, 0] = False
        lmp[:, -1] = False
    else:
        zonal = True

    lon_ls = lon[x_ls]  # not looped
    lat_ls = lat[y_ls]
    #rad_ls = rad[r_ls]
    r_ls = np.array([np.where(rad == i)[0][0] for i in rad_ls],dtype="i4")

    # convert deg to km
    dy = _deg2km_lat(dlat, rr)

    new_lon_ls = []
    new_lat_ls = [] 
    new_rad_ls = []
    ty_ls = []

    for l in range(len(x_ls)):

        i = x_ls[l]  # not looped
        j = y_ls[l]
        k = r_ls[l]
        
        # convert deg to km at each latitude
        dx = _deg2km_lon(dlon, lat_ls[l], rr)

        if zonal:  # stagger for looped
            i = i + 1

        f1 = np.zeros(9)  # middle r
        f1[0] = a[k, j, i]
        f1[1] = a[k, j-1, i-1]
        f1[2] = a[k, j-1, i]
        f1[3] = a[k, j-1, i+1]
        f1[4] = a[k, j, i+1]
        f1[5] = a[k, j+1, i+1]
        f1[6] = a[k, j+1, i]
        f1[7] = a[k, j+1, i-1]
        f1[8] = a[k, j, i-1]

        fx = (f1[4] - f1[8]) /dx
        fy = (f1[6] - f1[2]) /dy

        fxy = (f1[1] - f1[3] + f1[5] - f1[7]) / dx/dy

        fxx = np.float32(4.) * (f1[4] + f1[8] - np.float32(2.) * f1[0]) / dx/dx
        fyy = np.float32(4.) * (f1[2] + f1[6] - np.float32(2.) * f1[0]) / dy/dy

        d1 = fxx
        d2 = fxx * fyy - fxy * fxy

        if k == 0 or k == len(rad)-1:  # r boundary, 2d approximation

            # check definiteness of Hessian
            convex = d1 > 0 and d2 > 0 #and f1[0] < 0 # AS-
            concave = d1 < 0 and d2 > 0 #and f1[0] > 0  # AS+

            if not convex and not concave:  # not extrema
                continue

            x = ( fyy * (-fx) + (-fxy) * (-fy)) / d2
            y = (-fxy * (-fx) +   fxx  * (-fy)) / d2
            
            # convert km to deg
            ulat = _km2deg_lat(y, rr)
            ulon = _km2deg_lon(x, lat_ls[l]+ulat*np.float32(0.5), rr)

            if abs(ulat) > 45. or abs(ulon) > 45.:  # too much update induce eternal loop...
                ulat = np.float32(0.)
                ulon = np.float32(0.)

            if convex:
                ty_ls.append(1)
            elif concave:
                ty_ls.append(0)

            new_lon_ls.append(lon_ls[l] + ulon)
            new_lat_ls.append(lat_ls[l] + ulat)
            new_rad_ls.append(rad_ls[l])
            
            continue

        f0 = np.zeros(9)  # bottom r
        f0[0] = a[k-1, j, i]
        #f0[1] = a[k-1, j-1, i-1]
        f0[2] = a[k-1, j-1, i]
        #f0[3] = a[k-1, j-1, i+1]
        f0[4] = a[k-1, j, i+1]
        #f0[5] = a[k-1, j+1, i+1]
        f0[6] = a[k-1, j+1, i]
        #f0[7] = a[k-1, j+1, i-1]
        f0[8] = a[k-1, j, i-1]

        f2 = np.zeros(9)  # top r
        f2[0] = a[k+1, j, i]
        #f2[1] = a[k+1, j-1, i-1]
        f2[2] = a[k+1, j-1, i]
        #f2[3] = a[k+1, j-1, i+1]
        f2[4] = a[k+1, j, i+1]
        #f2[5] = a[k+1, j+1, i+1]
        f2[6] = a[k+1, j+1, i]
        #f2[7] = a[k+1, j+1, i-1]
        f2[8] = a[k+1, j, i-1]

        fz = (f2[0] - f0[0]) / dz

        fyz = (f0[2] - f0[6] + f2[6] - f2[2]) / dy/dz
        fzx = (f0[8] - f0[4] + f2[4] - f2[8]) / dz/dx

        fzz = np.float32(4.) * (f2[0] + f0[0] - np.float32(2.) * f1[0]) / dz/dz

        d3 = fxx*fyy*fzz + fxy*fyz*fzx*np.float32(2.) - fzx**np.float32(2.)*fyy - fxx*fyz**np.float32(2.) - fxy**np.float32(2.)*fzz

        # check definiteness of Hessian
        convex = d1 > 0 and d2 > 0 and d3 > 0 #and f1[0] < 0  # AS-
        concave = d1 < 0 and d2 > 0 and d3 < 0 #and f1[0] > 0  # AS+

        if not convex and not concave:  # not extrema
            continue

        # 3D update 
        x = (- (fyy*fzz - fyz**2) * fx - (fzx*fyz - fxy*fzz) * fy - (fxy*fyz - fzx*fyy) * fz) / d3
        y = (- (fyz*fzx - fxy*fzz) * fx - (fxx*fzz - fzx**2) * fy - (fzx*fxy - fxx*fyz) * fz) / d3
        z = (- (fxy*fyz - fyy*fzx) * fx - (fxy*fzx - fxx*fyz) * fy - (fxx*fyy - fxy**2) * fz) / d3

        '''# 2D update for x,y
        x = ( fyy * (-fx) + (-fxy) * (-fy)) / d2
        y = (-fxy * (-fx) +   fxx  * (-fy)) / d2
        # 3D update for z
        z = (- (fxy*fyz - fyy*fzx) * fx - (fxy*fzx - fxx*fyz) * fy - (fxx*fyy - fxy**2) * fz) / d3'''

        # prevent too much updates
        if rad_ls[l] + z < np.min(rad): new_rad = np.min(rad)
        elif rad_ls[l] + z > np.max(rad): new_rad = np.max(rad)
        else: new_rad = rad_ls[l] + z

        # convert km to deg
        ulat = _km2deg_lat(y, rr)
        ulon = _km2deg_lon(x, lat_ls[l]+ulat*np.float32(0.5), rr)

        if abs(ulat) > 45. or abs(ulon) > 45.:  # reject too much update
            ulat = np.float32(0.)
            ulon = np.float32(0.)

        if convex:
            ty_ls.append(1)
        elif concave:
            ty_ls.append(0)

        new_lon_ls.append(lon_ls[l] + ulon)
        new_lat_ls.append(lat_ls[l] + ulat)
        new_rad_ls.append(new_rad)

    #print('after hessian search', len(new_lon_ls))
    new_lon_ls = np.array(new_lon_ls, dtype='f4')
    new_lat_ls = np.array(new_lat_ls, dtype='f4')
    new_rad_ls = np.array(new_rad_ls, dtype='f4')

    lon_ls2, lat_ls2 = _correct_lon_lat(new_lon_ls, new_lat_ls, lon, lat, zloop)

    return lon_ls2, lat_ls2, new_rad_ls, np.array(ty_ls, dtype='i4')


@njit
def _search_off_grid2(a, lon, lat, lmp, rr, zloop):

    dlon = np.abs(lon[1] - lon[0])  # deg
    dlat = np.abs(lat[1] - lat[0])  # deg

    y_ls, x_ls = np.where(lmp)

    # remove y boundary (north/south pole)
    lmp[0, :] = False
    lmp[-1, :] = False
    # remove x boundary for not zonal
    if lon.shape[0] == a.shape[-1]:
        zonal = False
        lmp[:, 0] = False
        lmp[:, -1] = False
    else:
        zonal = True

    lon_ls = lon[x_ls]  # not looped
    lat_ls = lat[y_ls]

    # convert deg to km
    dy = _deg2km_lat(dlat, rr)

    new_lon_ls = []
    new_lat_ls = [] 
    ty_ls = []

    for l in range(len(x_ls)):

        i = x_ls[l]  # not looped
        j = y_ls[l]

        # convert deg to km at each latitude
        dx = _deg2km_lon(dlon, lat_ls[l], rr)

        if zonal:  # stagger for looped
            i = i + 1

        f1 = np.zeros(9)  # middle r
        f1[0] = a[j, i]
        f1[1] = a[j-1, i-1]
        f1[2] = a[j-1, i]
        f1[3] = a[j-1, i+1]
        f1[4] = a[j, i+1]
        f1[5] = a[j+1, i+1]
        f1[6] = a[j+1, i]
        f1[7] = a[j+1, i-1]
        f1[8] = a[j, i-1]

        fx = (f1[4] - f1[8]) / dx
        fy = (f1[6] - f1[2]) / dy

        fxy = (f1[1] - f1[3] + f1[5] - f1[7]) / dx/dy

        fxx = np.float32(4.) * (f1[4] + f1[8] - np.float32(2.) * f1[0]) / dx/dx
        fyy = np.float32(4.) * (f1[2] + f1[6] - np.float32(2.) * f1[0]) / dy/dy

        d1 = fxx
        d2 = fxx * fyy - fxy * fxy

        # check definiteness of Hessian
        concave = d1 > 0 and d2 > 0
        convex = d1 < 0 and d2 > 0

        if not concave and not convex:  # not exterma
            continue

        x = ( fyy * (-fx) + (-fxy) * (-fy)) / d2
        y = (-fxy * (-fx) +   fxx  * (-fy)) / d2

        # convert km to deg
        ulat = _km2deg_lat(y, rr)
        ulon = _km2deg_lon(x, lat_ls[l]+ulat*np.float32(0.5), rr)
        
        if abs(ulat) > 45. or abs(ulon) > 45.:  # too much update induce eternal loop...
            ulat = np.float32(0.)
            ulon = np.float32(0.)

        if convex:
            ty_ls.append(1)
        elif concave:
            ty_ls.append(0)

        new_lon_ls.append(lon_ls[l] + ulon)
        new_lat_ls.append(lat_ls[l] + ulat)
        
    new_lon_ls = np.array(new_lon_ls, dtype='f4')
    new_lat_ls = np.array(new_lat_ls, dtype='f4')

    lon_ls2, lat_ls2 = _correct_lon_lat(new_lon_ls, new_lat_ls, lon, lat, zloop)

    return lon_ls2, lat_ls2, np.array(ty_ls, dtype='i4')


@njit
def _correct_lon_lat(lon_ls, lat_ls, lon, lat, zloop):

    lonmax, lonmin = lon.max(), lon.min()
    dlon = lon[1] - lon[0]
    latmax, latmin = lat.max(), lat.min()

    new_lon_ls = np.zeros_like(lon_ls)
    new_lat_ls = np.zeros_like(lat_ls)

    for k in range(len(lon_ls)):

        nlon = lon_ls[k]
        nlat = lat_ls[k]

        # correct lon lat
        flag = True
        while flag:
            flag = False
            if zloop:  # zonal loop
                if nlon - dlon > lonmax:
                    nlon = nlon - np.float32(360.)
                    flag = True
                elif nlon < lonmin:
                    nlon = nlon + np.float32(360.)
                    flag = True
            else:
                if nlon > lonmax:
                    nlon = lonmax
                if nlon < lonmin:
                    nlon = lonmin
            if latmax == 90. and zloop:
                if nlat > latmax:  # beyond NorthPole
                    nlat = np.float32(180.) - nlat
                    nlon = nlon + np.float32(180.)
            else:
                if nlat > latmax:
                    nlat = latmax
            if latmin == -90. and zloop:
                if nlat < latmin:  # beyoond SouthPole
                    nlat = - np.float32(180.) - nlat
                    nlon = nlon + np.float32(180.)
            else:
                if nlat < latmin:
                    nlat = latmin

        new_lon_ls[k] = nlon
        new_lat_ls[k] = nlat

    return new_lon_ls, new_lat_ls


@njit
def search_localextrema_3d(f, maxmin='both', relax=2, zloop=False):

    ir, iy, ix = f.shape

    if zloop:    # zonal loop
        wide = np.empty((ir, iy, ix+2), dtype='f4')
        wide[:, :, 0] = f[:, :, -1]
        wide[:, :, 1:-1] = f[:, :, :]
        wide[:, :, -1] = f[:, :, 0]
        o = f[1:-1, 1:-1, :]  # center
        omin = f[0, 1:-1, :]  # center with min r
        omax = f[-1, 1:-1, :]  # center with max r
    else:
        wide = f
        o = f[1:-1, 1:-1, 1:-1]  # center
        omin = f[0, 1:-1, 1:-1]  # center with min r
        omax = f[-1, 1:-1, 1:-1]  # center with max r

    ir2, iy2, ix2 = o.shape

    # around all grids 
    stencil_num2 = 4+1
    stencil_num = 4+1+1 #8+5+5  # 8+9+9

    if maxmin == 'both' or maxmin == 'max':
        # make bool like original shape
        lmp = np.full((ir, iy, ix), False)

        a = np.empty((stencil_num, ir2, iy2, ix2))
        amin = np.empty((stencil_num2, iy2, ix2))
        amax = np.empty((stencil_num2, iy2, ix2))

        a[0] = np.where(wide[1:-1, 2:  , 1:-1] - o <= 0, 1, 0)  # n
        a[1] = np.where(wide[1:-1,  :-2, 1:-1] - o <= 0, 1, 0)  # s
        a[2] = np.where(wide[1:-1, 1:-1,  :-2] - o <= 0, 1, 0)  # w
        a[3] = np.where(wide[1:-1, 1:-1, 2:  ] - o <= 0, 1, 0)  # e
        #a[4] = np.where(wide[1:-1,  2:  , 2:  ] - o <= 0, 1, 0)  # ne
        #a[5] = np.where(wide[1:-1,   :-2, 2:  ] - o <= 0, 1, 0)  # se
        #a[6] = np.where(wide[1:-1,  2:  ,  :-2] - o <= 0, 1, 0)  # nw
        #a[7] = np.where(wide[1:-1,   :-2,  :-2] - o <= 0, 1, 0)  # sw
        # larger r
        a[4] = np.where(wide[2:, 1:-1, 1:-1] - o <= 0, 1, 0)  # oa
        # smaller r
        a[5] = np.where(wide[:-2, 1:-1, 1:-1] - o <= 0, 1, 0)  # ob
        S = a.sum(axis=0)  # includs low and high

        amin[0] = np.where(wide[0, 2:  , 1:-1] - omin <= 0, 1, 0)  # n
        amin[1] = np.where(wide[0,  :-2, 1:-1] - omin <= 0, 1, 0)  # s
        amin[2] = np.where(wide[0, 1:-1,  :-2] - omin <= 0, 1, 0)  # w
        amin[3] = np.where(wide[0, 1:-1, 2:  ] - omin <= 0, 1, 0)  # e
        amin[4] = np.where(wide[1, 1:-1, 1:-1] - omin <= 0, 1, 0)  # oa
        Smin = amin.sum(axis=0)
        amax[0] = np.where(wide[-1, 2:  , 1:-1] - omax <= 0, 1, 0)  # n
        amax[1] = np.where(wide[-1,  :-2, 1:-1] - omax <= 0, 1, 0)  # s
        amax[2] = np.where(wide[-1, 1:-1,  :-2] - omax <= 0, 1, 0)  # w
        amax[3] = np.where(wide[-1, 1:-1, 2:  ] - omax <= 0, 1, 0)  # e
        amax[4] = np.where(wide[-2, 1:-1, 1:-1] - omax <= 0, 1, 0)  # oa
        Smax = amax.sum(axis=0)

        if zloop:
            lmp[1:-1, 1:-1, :] = np.where(S>=stencil_num-relax, True, False)
            lmp[0, 1:-1, :] = np.where(Smin>=stencil_num2-relax, True, False)
            lmp[-1, 1:-1, :] = np.where(Smax>=stencil_num2-relax, True, False)
        else:
            lmp[1:-1, 1:-1, 1:-1] = np.where(S>=stencil_num-relax, True, False)
            lmp[0, 1:-1, 1:-1] = np.where(Smin>=stencil_num2-relax, True, False)
            lmp[-1, 1:-1, 1:-1] = np.where(Smax>=stencil_num2-relax, True, False)

        if maxmin == 'max':
            return lmp

    if maxmin == 'both' or maxmin == 'min':
        # make bool like original shape
        lmp2 = np.full((ir, iy, ix), False)

        b = np.empty((stencil_num, ir2, iy2, ix2))
        bmin = np.empty((stencil_num2, iy2, ix2))
        bmax = np.empty((stencil_num2, iy2, ix2))

        b[0] = np.where(wide[1:-1, 2:  , 1:-1] - o >= 0, 1, 0)  # n
        b[1] = np.where(wide[1:-1,  :-2, 1:-1] - o >= 0, 1, 0)  # s
        b[2] = np.where(wide[1:-1, 1:-1,  :-2] - o >= 0, 1, 0)  # w
        b[3] = np.where(wide[1:-1, 1:-1, 2:  ] - o >= 0, 1, 0)  # e
        #b[4] = np.where(wide[1:-1,  2:  , 2:  ] - o >= 0, 1, 0)  # ne
        #b[5] = np.where(wide[1:-1,   :-2, 2:  ] - o >= 0, 1, 0)  # se
        #b[6] = np.where(wide[1:-1,  2:  ,  :-2] - o >= 0, 1, 0)  # nw
        #b[7] = np.where(wide[1:-1,   :-2,  :-2] - o >= 0, 1, 0)  # sw
        # larger r
        b[4] = np.where(wide[2:, 1:-1, 1:-1] - o >= 0, 1, 0)  # oa
        # smaller r
        b[5] = np.where(wide[:-2, 1:-1, 1:-1] - o >= 0, 1, 0)  # ob
        S2 = b.sum(axis=0)  # includs low and high

        bmin[0] = np.where(wide[0, 2:  , 1:-1] - omin >= 0, 1, 0)  # n
        bmin[1] = np.where(wide[0,  :-2, 1:-1] - omin >= 0, 1, 0)  # s
        bmin[2] = np.where(wide[0, 1:-1,  :-2] - omin >= 0, 1, 0)  # w
        bmin[3] = np.where(wide[0, 1:-1, 2:  ] - omin >= 0, 1, 0)  # e
        bmin[4] = np.where(wide[1, 1:-1, 1:-1] - omin >= 0, 1, 0)  # oa
        Smin2 = bmin.sum(axis=0)
        bmax[0] = np.where(wide[-1, 2:  , 1:-1] - omax >= 0, 1, 0)  # n
        bmax[1] = np.where(wide[-1,  :-2, 1:-1] - omax >= 0, 1, 0)  # s
        bmax[2] = np.where(wide[-1, 1:-1,  :-2] - omax >= 0, 1, 0)  # w
        bmax[3] = np.where(wide[-1, 1:-1, 2:  ] - omax >= 0, 1, 0)  # e
        bmax[4] = np.where(wide[-2, 1:-1, 1:-1] - omax >= 0, 1, 0)  # oa
        Smax2 = bmax.sum(axis=0)

        if zloop:
            lmp2[1:-1, 1:-1, :] = np.where(S2>=stencil_num-relax, True, False)
            lmp2[0, 1:-1, :] = np.where(Smin2>=stencil_num-relax, True, False)
            lmp2[-1, 1:-1, :] = np.where(Smax2>=stencil_num-relax, True, False)
        else:
            lmp2[1:-1, 1:-1, 1:-1] = np.where(S2>=stencil_num-relax, True, False)
            lmp2[0, 1:-1, 1:-1] = np.where(Smin2>=stencil_num-relax, True, False)
            lmp2[-1, 1:-1, 1:-1] = np.where(Smax2>=stencil_num-relax, True, False)

        if maxmin == 'min':
            return lmp2

    return np.where(lmp+lmp2, True, False)

@njit
def _tripod_approx_point(n, s, e, w):
    """
    Approximate a missing point value
    with other valid three point values.
    """

    if np.isnan(n):
        n2 = e+w-s
        e2,w2,s2 = e,w,s
    elif np.isnan(s):
        s2 = e+w-n
        e2,w2,n2 = e,w,n
    elif np.isnan(e):
        e2 = n+s-w
        n2,s2,w2 = n,s,w
    elif np.isnan(w):
        w2 = n+s-e
        n2,s2,e2 = n,s,e
    else:
        n2,s2,e2,w2 = n,s,e,w

    return n2, s2, e2, w2

@njit
def _calc_sole_numba(da, lons, lats, lon, lat, rad,
                     zloop, stencil='9g', factor=np.float32(100.)):

    # make interp z
    if stencil == '9g':
        #ths = np.arange(np.float32(0.), np.float32(359.), np.float32(45.), dtype='f4')
        ths = np.array([0.,45.,90.,135.,180.,225.,270.,315.,], dtype='f4')
    else:
        #ths = np.arange(np.float32(0.), np.float32(359.), np.float32(90.), dtype='f4')
        ths = np.array([0.,90.,180.,270.], dtype='f4')

    drad = rad / 10
    rad += drad

    for i in range(10):
        rad -= drad

        around = np.empty_like(ths, dtype='f4')

        for l in range(len(ths)):
            
            if stencil == '9g':
                lon2, lat2 = invert_gcd2(lon, lat, ths[l], rad)
            else:
                lon2, lat2 = invert_gcd1(lon, lat, ths[l], rad)

            if zloop == 'loop0' or zloop == 'loop1':
                if zloop == 'loop0':
                    lon_max = lons[-1] + (lons[1] - lons[0])
                else: lon_max = lons[-1]
                lon_min = lons[0]

                if lon2 > lon_max: lon2 = lon2 - np.float32(360.)
                if lon2 < lon_min: lon2 = lon2 + np.float32(360.)

            around[l] = bilinear_interp(da, lons, lats, lon2, lat2)

        c = bilinear_interp(da, lons, lats, lon, lat)

        # calculate AS
        if stencil == '9g':
            e, ne, n, nw, w, sw, s, se = around

            # for nan included data, experimental
            #_e, _ne, _n, _nw, _w, _sw, _s, _se = around
            #n,s,e,w = _tripod_approx_point(_n,_s,_e,_w)
            #ne,sw,se,nw = _tripod_approx_point(_ne,_sw,_se,_nw)

            #if np.isnan([n,s,e,w,ne,sw,se,nw]).any():
            if np.isnan(n) or np.isnan(s) or np.isnan(e) or np.isnan(w) or np.isnan(ne) or np.isnan(sw) or np.isnan(se) or np.isnan(nw):
                continue

            # main parameter
            AS = (n+s+e+w+ne+nw+sw+se-np.float32(8)*c) / np.float32(8) / rad
            _m = (e-w)/np.float32(4)/rad + (ne+se-nw-sw)/np.float32(8)/rad/np.cos(np.pi/np.float32(4))
            _n = (n-s)/np.float32(4)/rad + (ne+nw-se-sw)/np.float32(8)/rad/np.cos(np.pi/np.float32(4))
            # ellips compression factor(ee) est by eign val of Hessian
            xx = (e+w-np.float32(2)*c)/rad/rad
            yy = (n+s-np.float32(2)*c)/rad/rad
            xy = (ne-nw-se+sw)/np.float32(4)/rad/np.cos(np.pi/np.float32(4))
            H = np.array([[xx, xy], [xy, yy]])
            if np.isnan(xx) or np.isnan(xy) or np.isnan(yy):
                ee = np.nan
            else:
                eigs = LA.eig(H)[0]
                axes = np.reciprocal(np.sqrt(np.abs(eigs)))
                ee = min(axes)/max(axes)

        else:
            e, n, w, s = around
            AS = (n+s+e+w-np.float32(4)*c) / np.float32(4) / rad
            _m = (e-w) / np.float32(2) / rad
            _n = (n-s) / np.float32(2) / rad
            xx = (e+w-np.float32(2)*c)/rad/rad
            ee = np.float32(1.)
            
        AS = AS * factor
        m = _m * factor
        n = _n * factor
        return c, AS, m, n, ee, xx
    return np.nan,np.nan,np.nan,np.nan,np.nan,np.nan

@njit
def _get_both(lmp_l,lmp_h,val):
    lmp3=lmp_l+lmp_h
    _l = np.where(lmp_l,val[0],np.float32(0.0))
    _h = np.where(lmp_h,val[1],np.float32(0.0))
    _b = _l + _h
    return _b.flatten()[np.where(lmp3.flatten())]

@njit
def _opt_params_numba(zloop, off_grid3, ty, stencil, factor,
                      _AS, _ro, _m, _n, _AS3,
                      da, lons, lats, lonSkp, latSkp,
                      r, rr, lev,
                      So_thres, Do_thres, SR_thres, xx_thres):

    zloop2 = 'loop' in zloop

    if off_grid3:
        lmp_l = search_localextrema(_AS[0], 'maximum', zloop=zloop2, as2=_AS[1])
        lmp_h = search_localextrema(_AS[1], 'minimum', zloop=zloop2, as2=_AS[0])
        lmp3 = lmp_l + lmp_h
        rad_ls = _get_both(lmp_l,lmp_h,_ro)

        # get off-grid points using 3d hessian
        if zloop2:
            ar32 = add_cyclic_point_hand3(_AS3)
        else:
            ar32 = _AS3

        # only ar3 is looped
        _ls_lon, _ls_lat, _ls_ro, _ls_ty = _search_off_grid3(ar32, lonSkp, latSkp, r, lmp3, rr, zloop2, rad_ls)

        #print('calc_sole')
        _ls_valV = np.empty_like(_ls_ro, dtype='f4')
        _ls_So = np.empty_like(_ls_ro, dtype='f4')
        _ls_m = np.empty_like(_ls_ro, dtype='f4')
        _ls_n = np.empty_like(_ls_ro, dtype='f4')
        _ls_ee = np.empty_like(_ls_ro, dtype='f4')
        _ls_xx = np.empty_like(_ls_ro, dtype='f4')

        for k in range(len(_ls_ro)):

            _rad = _ls_ro[k]
            _lat = _ls_lat[k]
            _lon = _ls_lon[k]

            _ret = _calc_sole_numba(da, lons, lats,
                                    _lon, _lat, _rad,
                                    zloop, stencil, factor)
            _valV, _So, _m, _n, _ee, _xx = _ret

            _ls_valV[k] = _valV
            _ls_So[k] = np.abs(_So)
            _ls_m[k] = _m
            _ls_n[k] = _n
            _ls_ee[k] = _ee
            _ls_xx[k] = np.abs(_xx * np.float32(1e4))

        _is_not_nan = ~np.isnan(_ls_So)
        ls_valV = _ls_valV[_is_not_nan]
        ls_So = _ls_So[_is_not_nan]
        ls_m = _ls_m[_is_not_nan]
        ls_n = _ls_n[_is_not_nan]
        ls_ee = _ls_ee[_is_not_nan]
        ls_xx = _ls_xx[_is_not_nan]
        ls_lon = _ls_lon[_is_not_nan]
        ls_lat = _ls_lat[_is_not_nan]
        ls_ro = _ls_ro[_is_not_nan]
        ls_ty = _ls_ty[_is_not_nan]

    elif ty == "L" or ty == "H":
        if ty == 'L':
            AS = _AS[0]
            ro = _ro[0]
            m = _m[0]
            n = _n[0]
            lmp = search_localextrema(AS, 'maximum', zloop=zloop2, as2=_AS[1])
        elif ty == 'H':
            AS = _AS[1]
            ro = _ro[1]
            m = _m[1]
            n = _n[1]
            lmp = search_localextrema(AS, 'minimum', zloop=zloop2, as2=_AS[0])
        ls_y, ls_x = np.where(lmp)
        ls_lat = latSkp[ls_y]
        ls_lon = lonSkp[ls_x]
        lmp_flat = lmp.flatten()
        ls_valV = da.flatten()[np.where(lmp_flat)]
        ls_So = AS.flatten()[np.where(lmp_flat)]
        ls_ro = ro.flatten()[np.where(lmp_flat)]
        ls_m = m.flatten()[np.where(lmp_flat)]
        ls_n = n.flatten()[np.where(lmp_flat)]
        if ty == "L":
            ls_ty = np.full(ls_n.shape, 0, dtype="i4")
        elif ty == "H":
            ls_ty = np.full(ls_n.shape, 1, dtype="i4")

        ls_ee = np.ones_like(ls_ro, dtype='f4')
        ls_xx = np.ones_like(ls_ro, dtype='f4')

    elif ty == 'both':
        lmp_l = search_localextrema(_AS[0], 'maximum', zloop=zloop2, as2=_AS[1])
        lmp_h = search_localextrema(_AS[1], 'minimum', zloop=zloop2, as2=_AS[0])
        lmp3 = lmp_l + lmp_h
        ls_y, ls_x = np.where(lmp3)
        ls_lat = latSkp[ls_y]
        ls_lon = lonSkp[ls_x]
        ls_So = _get_both(lmp_l,lmp_h,_AS)
        ls_ro = _get_both(lmp_l,lmp_h,_ro)
        ls_m = _get_both(lmp_l,lmp_h,_m)
        ls_n = _get_both(lmp_l,lmp_h,_n)
        ls_valV = da.flatten()[np.where(lmp3.flatten())]
        _ty_l = np.where(lmp_l,0,0).astype("i4")
        _ty_h = np.where(lmp_h,1,0).astype("i4")
        _ty = _ty_l + _ty_h
        ls_ty = _ty.flatten()[np.where(lmp3.flatten())]

        ls_ee = np.ones_like(ls_ro, dtype='f4')
        ls_xx = np.ones_like(ls_ro, dtype='f4')

    ls_So = np.abs(ls_So)  # positive for H
    ls_Do = ls_So * ls_ro / factor  #[gpm]
    ls_SBG = (ls_m**np.float32(2.)+ls_n**np.float32(2.))**np.float32(.5) #[gpm/100km]
    ls_SBGang = np.arctan2(ls_n, ls_m)  #[rad]
    ls_SR = ls_SBG / ls_So

    ls_lev = np.full(ls_lon.shape, lev, dtype='i4')

    ## noise reduction constraints ###########
    idx = np.full((len(ls_lon)), True)
    for i in range(len(ls_lon)):
        # remove weak depression
        if ls_So[i] < So_thres:
            idx[i] = False; continue
        # remove shallow noise (mainly due to orography)
        if ls_Do[i] < Do_thres:
            idx[i] = False; continue
        # remove weak wave
        if ls_SR[i] > SR_thres:
            idx[i] = False; continue
        # remove high-ellipticity depression
        #if ls_ee[i] < ee_thres:
        #    idx[i] = False; continue
        # remove zonally plain depression
        if ls_xx[i] < xx_thres:
            idx[i] = False; continue
        # remove too small/large ones, optional  # depricated
        #if rm_small and ls_ro[i] == rmin:
        #    idx[i] = False; continue
        #if rm_large and ls_ro[i] == rmax:
        #    idx[i] = False; continue

    # drop values
    ls_ty_0 = ls_ty[idx]
    ls_lev_0, ls_lat_0, ls_lon_0 = ls_lev[idx], ls_lat[idx], ls_lon[idx]
    ls_valV_0 = ls_valV[idx]
    ls_So_0, ls_ro_0, ls_Do_0 = ls_So[idx], ls_ro[idx], ls_Do[idx]
    ls_SBG_0, ls_SBGang_0 = ls_SBG[idx], ls_SBGang[idx]
    ls_m_0, ls_n_0, ls_SR_0 = ls_m[idx], ls_n[idx], ls_SR[idx]
    ls_ee_0, ls_xx_0 = ls_ee[idx], ls_xx[idx]

    ### ro-area unique local maximum test (yamane scheme)
    # descending sort along So
    idx0 = np.argsort(ls_So_0)[::-1]
    ls_ty_1 = ls_ty_0[idx0]
    ls_lev_1, ls_lat_1, ls_lon_1 = ls_lev_0[idx0], ls_lat_0[idx0], ls_lon_0[idx0]
    ls_valV_1 = ls_valV_0[idx0]
    ls_So_1, ls_ro_1, ls_Do_1 = ls_So_0[idx0], ls_ro_0[idx0], ls_Do_0[idx0]
    ls_SBG_1, ls_SBGang_1 = ls_SBG_0[idx0], ls_SBGang_0[idx0]
    ls_m_1, ls_n_1, ls_SR_1 = ls_m_0[idx0], ls_n_0[idx0], ls_SR_0[idx0]
    ls_ee_1, ls_xx_1 = ls_ee_0[idx0], ls_xx_0[idx0]

    # main loop of yamane scheme to remove extremely near pair
    idx1 = np.full((len(ls_lon_1)), True)
    for i in range(len(ls_lon_1)):
        if not idx1[i]: continue
        for j in range(i+1, len(ls_lon_1)):
            if ls_ty_1[i] != ls_ty_1[j]: continue
            dist = great_circle_distance_numba(
                    ls_lon_1[i], ls_lat_1[i],
                    ls_lon_1[j], ls_lat_1[j])
            if dist < ls_ro_1[i] and dist < ls_ro_1[j]:
                if ls_So_1[j] <= ls_So_1[i]:
                    idx1[j] = False  # not area local maximum
                    idx1[i] = True  # rescure the other

    # drop values and
    # all in one dictionary for DataFrame as point data
    return (ls_ty_1[idx1], ls_lev_1[idx1], ls_lat_1[idx1], ls_lon_1[idx1],
            ls_valV_1[idx1], ls_So_1[idx1], ls_ro_1[idx1], ls_Do_1[idx1],
            ls_SBG_1[idx1], ls_SBGang_1[idx1], ls_m_1[idx1], ls_n_1[idx1],
            ls_SR_1[idx1], ls_ee_1[idx1], ls_xx_1[idx1])


@njit
def _input_ex_numba(zloop, daSkp, lonSkp, latSkp, off_grid3, rr,
                    dai, loni, lati, ty, lev,
                    lsV_ty, lsV_lon, lsV_lat, lsV_ro):
    zloop2 = 'loop' in zloop

    #if off_grid3:
    if False:
        lmp = search_localextrema(daSkp, zloop=zloop2)
        ls_lon, ls_lat, ls_ty = _search_off_grid2(daSkp, lonSkp, latSkp, lmp, zloop2, rr)
        ls_val = np.empty_like(ls_lon)
        for i in range(len(ls_lon)):
            ls_val[i] = bilinear_interp(dai, loni, lati,
                                        ls_lon[i], ls_lat[i])
    #else:
    if True:
        if ty == 'L' or ty == 'both':
            lmp = search_localextrema(daSkp, 'minimum', zloop=zloop2)
            ls_y_l, ls_x_l = np.where(lmp)
        if ty == 'H' or ty == 'both':
            lmp = search_localextrema(daSkp, 'maximum', zloop=zloop2)
            ls_y_h, ls_x_h = np.where(lmp)
        # make point data
        if ty == "L":
            ls_y, ls_x = ls_y_l, ls_x_l
        elif ty == "H":
            ls_y, ls_x = ls_y_h, ls_x_h
        else:
            ls_y = np.hstack((ls_y_l,ls_y_h))
            ls_x = np.hstack((ls_x_l,ls_x_h))

        if ty == "L":
            ls_ty = np.full(ls_x.shape, 0, dtype="i4")
        elif ty == "H":
            ls_ty = np.full(ls_x.shape, 1, dtype="i4")
        else:
            ls_ty_l = np.full(ls_x_l.shape, 0, dtype="i4")
            ls_ty_h = np.full(ls_x_h.shape, 1, dtype="i4")
            ls_ty = np.hstack((ls_ty_l,ls_ty_h))
        ls_lat, ls_lon = latSkp[ls_y], lonSkp[ls_x]
        # extract field values at points
        #ls_val = f[(np.where(lmp))]
        ls_val = daSkp.flatten()[np.where(lmp.flatten())]

    _shape = ls_lon.shape
    il = len(ls_lon)
    ls_lev = np.full(_shape, lev, dtype='i4')
    lsV_ex = np.zeros_like(lsV_ty, dtype='i4')
    lsV_valX = np.zeros_like(lsV_ty, dtype='f4')
    ilV = len(lsV_ty)

    #x_list = []
    x_list = np.full(_shape, False)
    for i in range(ilV):
        dist_min = np.float32(100000.)

        min_j = -1
        for j in range(il):

            if lsV_ty[i] != ls_ty[j]: continue
            if lsV_lat[i] * ls_lat[j] < np.float32(0.):  continue

            dist = great_circle_distance_numba(
                    lsV_lon[i], lsV_lat[i],
                    ls_lon[j], ls_lat[j])

            if dist < lsV_ro[i]*np.float32(0.63):  # =DR_c
                #self.V.at[i, 'ex'] = 1  # update V
                #self.V.at[i, 'valX'] = ls_val[j]
                lsV_ex[i] = 1
                lsV_valX[i] = ls_val[j]
                if dist < dist_min:
                    dist_min = dist
                    min_j = j

        if not min_j == -1:
            #x_list.append(min_j)  # reject irrelevant
            x_list[min_j] = True
    return (ls_ty[x_list], ls_lev[x_list], ls_lat[x_list], ls_lon[x_list],
            ls_val[x_list], lsV_ex, lsV_valX)
