# colindex2

<div align="left">
  <img src="map.png" width="40%">
</div>

Atmospheric depression detection scheme written as a Python package. The main target is to capture upper tropospheric depressions such as cutoff lows and blockng highs from their earlier stages of troughs and ridges, respectively, with their seamless transitions of vortex-related variables.

We are grateful if you cite below when you publish papers using this scheme.

- Kasuga, S., M. Honda, J. Ukita, S. Yaname, H. Kawase, A. Yamazaki, 2021: Seamless detection of cutoff lows and preexisting troughs. *Monthly Weather Review*, **149**, 3119–3134, [https://doi.org/10.1175/MWR-D-20-0255.1](https://doi.org/10.1175/MWR-D-20-0255.1)  


:warning: The tracking scheme is underconstruction and its definition may be changed unnoticed. The relatively stabe versions are v2.7.6 and v2.7.8 See Tags and CHANGELOG. (last edit: 2025.2.17)

# Install package and shell commands

```
pip install git+https://gitlab.com/kasugab3621/colindex2.git
```

Dependecies are `numpy`, `pandas`, `numba`, `xarary`, `netCDF4`, and `h5netcdf`. 

> If you use anaconda environment, prepare the dependencies using `conda` in advance and then install `colindex2` using conda's `pip`, as recommended by [ANACONDA](https://www.anaconda.com/blog/using-pip-in-a-conda-environment).  

<!--

- Git clone to use as a script that can run in the command line (pending...).
```
git clone https://gitlab.com/kasugab3621/colindex2.git
```

Requirements for Git clone uses
-  Python (above version 3.6)
-  [Numpy](https://numpy.org/doc/stable/user/whatisnumpy.html)
-  [Numba](https://numba.pydata.org)
-  [Pandas](https://pandas.pydata.org/docs/getting_started/overview.html)
-  [Xarray](https://docs.xarray.dev/en/stable/#)

-->

# For command line use

### 1. Generate data_settings.py

```
$ gen_data_settings
```

### 2. Edit [data_settings.py](data_settings.py)

### 3. Run detection

```
$ detect z.nc
```

### 4. Run tracking

```
$ track
```
Output data will be stored, assuming `output_dir = "d01"`, as follows..

```
current_dir/
└── d01/
    ├── AS/  # 2D averaged slope function
    |   └── AS-{ty}-{yyyymmddhhTT}-{llll}.nc (or .grd)
    ├── V/   # point values for detected depressions
    |   └── V-{ty}-{yyyymmddhhTT}-{llll}.csv
    ├── Vt/  # intermediate data
    |   └── V-{ty}-{yyyymmddhhTT}-{llll}.csv
    ├── Vtc/ # resultant point values for tracked depressions
    |   └── V-{ty}-{yyyymmddhhTT}-{llll}.csv
    └── X/   # point values for input extrema
        └── X-{ty}-{yyyymmddhhTT}-{llll}.csv
```
where, `ty` is `L` or `H`, `yyyymmddhhTT` is timestep, and `llll` is level in 4 digits.  

List of point-value output parameters are [here](md/variables.md). To retrieve the CSV for each track, please use `find_track` below.

### A. Draw maps to check tracking data and AS (netcdf output only)

```
$ draw_map z.nc ./d01 L 300 nps
```

### B. Find and make each tracking data

```
$ find_track ./d01 L 300 a
```


See also details of commands options shown by...
```
$ detect -h
$ track -h
$ draw_map -h
$ find_track -h
```

# For import use
### [documents](https://kasugab3621.gitlab.io/colindex2/index.html)


