
| Names| Description |
| ---- | ----------- |
| time | Time. Use `pd.read_csv('path/to/file.csv', parse_dates=['time']) |`
| ty | 0 for lows and troughs, 1 for highs and ridges |
| lev | Level. Usually in hPa. |
| lat, lon | Central latitude and longitude of depressions. |
| valV | Value of input field on the point |
| valX | Value of the nearest local minimum (maximum) of input field for a cutoff low (high). |
| So | Optimal slope [m/100 km]. Intensity of depresion (as circular geostrophic winds). |
| ro | Optimal radius [r km]. Size of depression (as a radius of the circulation). |
| Do | Optimal depth [m]. Vertical depth of depression, generally proportional to ro. |
| SBG | Background slope [m/100 km]. |
| SBGang | Angle of Background slope vector [radian]. 0 for east. |
| m, n | Zonal, meridional components of SBG, respectivelly [m/100 km]. |
| SR | Slope ratio. For analytic characters of a Gaussian-shaped depression, 0.-1.34 for vortices, 1.34- for waves, and larger values (e.g., 3.) for ripples in a jet. |
| ex | Distinction between depressions and waves. <br>1 (there is a extremum within ro\*0.65) for lows/highs and 0 (otherwise) for troughs/ridges. |
| EE | Eccentricity (1 for pure isotropic, lesser values for Ellipses, 0-1). |
| XX | Zonal discrete laplacian with a step of ro [m/(100 km)**2]. Small value means the depression is zonally uniform like sub-tropical ridges. |
| ID | Identification number. |
| MERGE | Merge lysis flag. <br>`-1` for soritary lysis, <br>`-2` for being merged from someone, <br>`-3` for lysis at the end of analysis, <br>other integers for the object ID of its merge lysis. |
| SPLIT | Split genesis flag. <br>`-1` for soritary genesis, <br>`-2` for being splitting and producing someone, <br>`-3` for genesis at the start of analysis, <br>other integers for the object ID of its split genesis. |
| DIST | Moving distance in a timestep [km]. Central difference. When merge/split/genesis/lysis, value will be missing, and the same applies to SPEED and DIR. |
| SPEED | Moving speed [m/s]. DIST/timestep. |
| DIR | Moving direction [radian]. 0 for east. |
| \_ DC | Accumulated duration including before split. |
| DU | Duration [hour]. |
| XS | Sequential duration being ex=1 (lows/highs). |
| QS5 | 1 for Quasi-stational with 85% temporal overlapping of ro circles. |
| QS7 | As with QS5 but with 70%. |
| ALLDIST | Accumulated moving distance [km]. |
| MEDDIST | Median moving distance of all timesteps [km]. |
| MAX | 1 for the maximum development timestep (maximum So). |
| MAXSo | So when MAX. |
| MAXXS | Maximum duration of XS in a track (XS can be scored in multiple sequences). |
| MAXQS5 | Maximum duration of QS5 in a track. |
| MAXQS7 | Maximum duration of QS7 in a track. |
| exGEN | Genesis as vortex (`ex` changed from `0` to `1` for t-1 to t). |
| exLYS | Lysis as vortex (`ex` changed from `1` to `0` for t to t+1). |
