# 寿命の数え上げスクリプト [count_duration.py](../count_duration.py)
個々の渦の寿命を取得し各タイムステップのcsvファイルに付与します．

追跡スクリプト同様，../track_settings.pyのCOUNTING SETTINGSを編集してください．

- HIGH_LOW (data_settings.pyと同様，低気圧(`'L'`)か高気圧(`'H'`)を選択します．)
- tracking_times (トラッキングをするタイムステップ)
- read_csv_duration (monthly: 読み込むデータを制限します．all: 全データを一度に読み込みます)
- MAXSo_thres (gpm/100 km，最大発達時の強度の閾値です．)
- DU_thres (hour，寿命の閾値です．)
- ALLDIST_thres (km，積算移動距離の閾値です．特に地形のノイズ除去に有効です)

ファイル内に詳細設定があります

- addmonth (`read_csv_duration=monthly`の場合，読み込むデータの幅を月で指定します，現象の寿命より十分長く設定してください)  

実行コマンドは以下です

```
./count_duration.py detect 200
```

この場合，`detect/Vt`からトラッキングデータを読み込み`detect/Vtc`に出力されます．  

# パラメータその１：寿命関係の基礎データ
出力データは，追跡データにさらに寿命の情報を追加したものです．

|DU|XS|QS5|QS7|
|:-:|:-:|:-:|:-:|
|渦の存続したhour|半径\*0.63円内に極点が連続して存在したhour|領域50%重複が連続（準停滞）したhour|領域70%重複が連続（準停滞）したhour|

## 寒冷渦の条件 [Munoz et al. 2020](https://doi.org/10.1175/JCLI-D-19-0497.1)
XS >= 36 (hour)

## ブロッキング高気圧の条件 [Schwierz et al. 2004](https://doi.org/10.1029/2003GL019341)
QS5 >= 24\*5 (hour)

# パラメータその２：ライフサイクルを踏まえたデータ
以下はMAX以外は全ての時刻で同じ値が保存されます．（どの時刻で読み込んでもライフサイクルの特徴がわかる）

|ALLDIST|MEDDIST|MAX|MAXSo|MAXro|MAXSb|
|:-:|:-:|:-:|:-:|:-:|:-:|
|積算移動距離km|移動距離中央値km|最大発達時刻|その強度gpm/100km|そのサイズkm|その背景場gpm/100km|

MAXはライフサイクルの中での最大強度で`1`，それ以外で`0`となります．ピークが何回あっても最大時しか記録されません．
MAX=1の際の強度・サイズ・背景場が保存されますが，サイズの最大や背景場の最大は保存されません．

<!--
※元々のバージョン（v0.5）では，3ヶ月の幅で1ヶ月ずらしの窓で渦の寿命を数え上げています．v0.6は2015年4月の1月分の抽出データへ取り急ぎ最適化したものです．また，内部でtry,exceptを多用しており，多分バギーです．このスクリプトに限らずバグ・エラーありましたらissuesでcreate issueしていただけると幸いです．
-->
