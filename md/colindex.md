# 抽出スクリプト [colindex.py](../colindex.py)

まず，netcdfを読み込む場合は[nc_settings.py](../nc_settings.py)を，バイナリを読み込む場合は[grd_settings.py](../grd_settings.py)を編集してください．それらと，modules.py, constants.pyが同じディレクトリにあることを確認してください．

以下のいずれかのコマンドで実行します．
```
./colindex.py dir
./colindex.py dir -i data.nc
```
ディレクトリ(`dir`)を引数に与えます．`dir`が生成され内部に出力されます．入力データはdata_settings.pyの`infile`に指定するか，`-i`オプションにより指定します．並列処理数はdata_settings.pyの`CPU`に指定するか，`-n`オプションでも指定可能です．  

grdデータを利用する場合，`-t`オプションによりyyyymmddhhTTのフォーマットで初期時刻を指定できます(静的にはgrd_settings.pyの`inittime`で指定します)．例えば１ヶ月にまとめられたgrdデータを利用する場合，
```
./colindex.py dir -t 199101010000 -n 31
./colindex.py dir -t 199102010000 -n 28
```
など，入力データに合わせて並列処理を最適化させることもできます．

<!--
サンプルデータを使ってデモンストレーションをします．まず以下のコマンドを実行します．この場合，data_settings.pyののinfile(入力ファイルパス), inittime(データ開始時刻)が参照されます．
```
./colindex.py -o detect
```
-oで出力ディレクトリを指定します(detectディレクトリが生成します)．指定しないとカレンとディレクトリに出力します．同時にdata_settings.pyのコピーも保存するので，抽出の設定を後から確認することができます．  
オプションを利用することでも入力ファイルパス（-i），開始時刻（-t）を10桁で指定可能です．
```
./colindex.py detect -i sample/Z2015041300-1406.bin -t 2015041312
```
この場合data_settings.pyに書かれているinfileとinittimeは無視されます．
-->

# 出力ファイル
基本的には3種類あり，V,X,ASというディレクトリにそれぞれ保存されます．yyyymmddhhは年4桁月2桁日2桁時2桁です．llllは気圧面をhPaで4桁です．-L-は低気圧，-H-は高気圧を指しますです．  

## V/yyyymm/V-L-yyyymmddhh-llll.csv, V/yyyymm/V-H-yyyymmddhh-llll.csv
抽出点のデータです．各フィールドの簡単な説明を以下の表にまとめます．

|time|lat|lon|y  |x  |val|So |ro |Do |Sb |ang|m  |n  |SR |ex|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|時刻|緯度|経度|Y|X|抽出点の入力値|最適傾き|最適半径|最適深度|背景傾き|背景勾配の向き|背景勾配の東西成分|南北成分|傾き比|極点判定|

So=optimal slope(最適傾き)，ro=optimal radius(最適半径)，Do=optimal depth(最適深度)，Sb=local background slope(背景傾き; 背景勾配の大きさ)，ang=$`\alpha`$(背景勾配の向き)，SR=Slope ratio(傾き比=背景傾き/最適傾き)，極点判定は低気圧（高気圧）の最適半径\*0.63の円内に極小（大）点を捜索し，1:あり, 0:なし，です（対流圏中層上層の低気圧抽出の場合，ex=0はトラフ，ex=1は寒冷渦）．詳細は参考文献を参照してください．


## X/yyyymm/X-L-yyyymmddhh-llll.csv, X/yyyymm/X-H-yyyymmddhh-llll.csv
極小点のデータです．最適半径\*0.63の内部かつ抽出点最近某のもののみ抽出します．各フィールドの説明を以下の表にまとめます．

|時間|lat|lon|y  |x  |val|
|:-:|:-:|:-:|:-:|:-:|:-:|
|時間|緯度|経度|Y|X|極点の入力値|

## AS/yyyymm/AS-L-yyyymmddhh-llll.nc, AS/yyyymm/AS-H-yyyymmddhh-llll.nc
低気圧抽出では$`AS^+`$，高気圧抽出では$`AS^-`$を出力します．Pythonで利用する場合はxarray.open_datasetしてください．GrADSで利用する場合はsdfopenしてください．

