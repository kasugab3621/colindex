netcdfを利用する場合はnc_settings.pyを，バイナリを利用する場合はgrd_settings.pyをデータに合わせて編集します．colindex.pyとcount_duration.pyで利用されます．

# nc_settings.pyの編集

変数名と各次元のshortnameを記入します．欠損値(_FillValue)はnp.nanとして処理されます．

# grd_settings.pyの編集

基本的にはctlファイルに書くようなことを記入します．

- fmt_grd  
エンディアンとバイトサイズを指定します．big_endian, 4byte floatなら`fmt_grd = '>4f'`，little_endian, 4byte floatなら`fmt_grd = '<4f'`．詳細は[Numpy dtype](https://numpy.org/doc/stable/reference/arrays.dtypes.html)．  

- inittime  
年4桁，月2桁，日2桁，時2桁，分2桁の12桁で初期時刻を指定します．

- freq  
pd.date_rangeの引数として利用します．年は`YS`，月は`MS`，日は`D`，時は`H`，分は`T`，を使います．数字なしの場合，1となります（`freq = 'H'`は1時間間隔）．詳細は[pandas User Guide](https://pandas.pydata.org/docs/user_guide/timeseries.html#offset-aliases)．  

- lon, lat, levs  
それぞれの座標列に対応したndarrayを指定します．lonとlatは`np.arange()`を利用します．これは，Python標準関数の`range()`のNumpy版で，小数点を含む数列を作成でき，ndarrayを返すのでNumpyのメソッドと相性が良いです．ガウシアングリッドなど，等間隔でない座標系の場合，levs同様に`np.array([lat0, lat1, .., latn])`とベタ打ちで書き込みます．

- undef_grd
欠損値の値を指定します．

- ix, iy, iz
lon, lat, levsの要素数です．