# ID確認用の作図スクリプト [map_check_ID.py](../map_check_ID.py)

作成中です．．

<!--
下記の項目を編集します．

- data_times: データのタイムステップ全体
- draw_times: 描画したいタイムステップ
- lev: 描画したい気圧面
- mproj: nps or sps or area
- ty: 'L' or 'H'
- idir: 追跡データのディレクトリ
- asdir: 抽出データのディレクトリ
- zfile: ソースのジオポテンシャル高度データのパス
- fdir: 出力画像ファイルのディレクトリ

引数なしで実行します．
```
./map_check_ID.py
```

idirへasdirと同じパスを指定することで，追跡前のデータでも描画可能です．indexmap.pyと異なり，一部の時刻を切り取って描画することができます．  

pngファイルを眺めて気になる渦を見つけたら，「年月日」と「ID」を控えます．  

## 任意トラックおよび全てのトラックを取り出すスクリプト [get_IDfile.py](../get_IDfile.py)

2つの使い方が可能です．

### 1. 任意のIDを指定する方法

map_check_ID.pyで控えた「年月日」と「ID」を利用します．
monthlyデータではない場合，内部で`monthly = False`とし，`data_times`にデータのタイムステップ（もしくは側索時間を限定しても良い）を指定してください．  
下記により，サンプルの寒冷渦（日本上空を通過するもの）のトラック（IDファイル）を取得し，idsというディレクトリに保存します．

```
./get_IDfile.py count ids 'L' 20150413 1
```

こちらの方法は，簡易的なモジュールとして利用可能です．呼び込むファイルと同じディレクトリへget_IDfile.pyをコピーし，下記のように利用してください．
```
from get_IDfile import get_IDfile
_ = get_IDfile(idir, odir, ty, lev, ymd, ID)
```
IDファイルのDataFrameを返しますが，データは関数内でodirへ保存されるのでただの保険？です．

### 2. 出現する全てのIDに対しIDファイルを生成する方法

ymdとIDに'all'と与えます．長期解析に利用するとサーバのノード数に余裕があったとしてもlsで固まる事態に陥る可能性がありますので注意．
```
./get_IDfile.py count ids 'L' 'all' 'all'
```
-->

