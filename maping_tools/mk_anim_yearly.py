#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
import warnings
warnings.simplefilter('ignore')

def update(z, odir='d01'):

    lev = z.lev.values
    t = pd.to_datetime(z.time.values)
    ym = f'{t.year}{t.month:02}'
    tn = f'{ym}{t.day:02}{t.hour:02}00'
    print(tn)

    ASp = xr.open_dataarray(f'./{odir}/AS/{ym}/AS-L-{tn}-{lev:04}.nc')
    ASn = xr.open_dataarray(f'./{odir}/AS/{ym}/AS-H-{tn}-{lev:04}.nc')
    AS = xr.where(ASp**2>ASn**2, ASp, ASn)

    VL = pd.read_csv(f'./{odir}/V/{ym}/V-L-{tn}-{lev:04}.csv', parse_dates=['time'])
    VH = pd.read_csv(f'./{odir}/V/{ym}/V-H-{tn}-{lev:04}.csv', parse_dates=['time'])
    #print(VL.columns)
    #quit()
    #'time', 'ty', 'lev', 'lat', 'lon', 'valV', 'valX', 'So',
    #'ro', 'Do', 'SBG', 'SBGang', 'm', 'n', 'SR', 'ex', 'ID', 'MERGE',
    #'SPLIT', 'DIST', 'SPEED', 'DIR', '_ID_c', '_SC', '_DC', '_LONs_B',
    #'_LATs_B', 'DU', 'XS', 'QS5', 'QS7', 'ALLDIST', 'MEDDIST', 'MAX',
    #'MAXSo', 'MAXXS', 'MAXQS5', 'MAXQS7'

    # constraints
    #print(len(vl), len(vh))
    #vl = vl[vl.maxso>15]
    #vh = vh[vh.maxso>15]
    #vl = vl[vl.maxxs>1.5*24]
    #vh = vh[vh.maxxs>1.5*24]
    #print(len(vl), len(vh))
    #quit()
    # divides
    VL = VL[VL.lat>0]
    VH = VH[VH.lat>0]
    VL_weak = VL[VL.So<15.]
    VH_weak = VH[VH.So<15.]
    VL = VL[VL.So>=15.]
    VH = VH[VH.So>=15.]
    TR = VL[VL.SR>1.34]
    CL = VL[VL.SR<=1.34]
    RG = VH[VH.SR>1.34]
    CH = VH[VH.SR<=1.34]
    TR_weak = VL_weak[VL_weak.SR>1.34]
    CL_weak = VL_weak[VL_weak.SR<=1.34]
    RG_weak = VH_weak[VH_weak.SR>1.34]
    CH_weak = VH_weak[VH_weak.SR<=1.34]
    #TR = VL[VL.ex==0]
    #CL = VL[VL.ex==1]
    #RG = VH[VH.ex==0]
    #CH = VH[VH.ex==1]
    #TR_weak = VL_weak[VL_weak.ex==0]
    #CL_weak = VL_weak[VL_weak.ex==1]
    #RG_weak = VH_weak[VH_weak.ex==0]
    #CH_weak = VH_weak[VH_weak.ex==1]

    z = z.sel(lat=slice(0, 90))
    AS = AS.sel(latitude=slice(0, 90))
    lat_as = AS.latitude.values
    AS, lon_as = add_cyclic_point(AS.squeeze().values,
                                  coord=AS.longitude.values)

    ax.cla()
    ax.coastlines(lw=2.5, ec='lightgray', alpha=.25)
    #ax.coastlines(fc='lightgray', ec='none', alpha=.5)
    ax.gridlines(xlocs=np.arange(-180,180,5),ylocs=np.arange(-85,90,5),
                  linewidths=.05, alpha=.5, zorder=5)

    #aslevs = np.arange(-40, 41, 5)
    #aslevs = np.delete(aslevs, aslevs==0)
    aslevs = [-35, -25, -15, -5, 5, 15, 25, 35]
    shade = ax.contourf(lon_as, lat_as, AS, aslevs,
                        extend='both', cmap='bwr_r', alpha=.5,
                        transform=ccrs.PlateCarree())
    if t.month == 1 and t.day == 1 and t.hour == 0:
        plt.colorbar(shade, shrink=.8,)# orientation='horizontal')
        return

    lat_z = z.lat.values
    z, lon_z = add_cyclic_point(z.values, coord=z.lon.values)
    zlevs = np.arange(z.min()//100*100, z.max()//100*100, 100)
    zlevs100 = np.delete(zlevs, zlevs%200==0)
    zlevs200 = np.delete(zlevs, zlevs%200!=0)
    contour = ax.contour(lon_z, lat_z, z, zlevs200, colors='k',
                         linewidths=1., transform=ccrs.PlateCarree(), zorder=6)
    plt.clabel(contour, fontsize=9)
    ax.contour(lon_z, lat_z, z, zlevs100, colors='k', linewidths=.3,
               transform=ccrs.PlateCarree())

    #Vs = [VL_weak, VH_weak]
    Vs = [TR_weak, CL_weak, RG_weak, CH_weak]
    colors = ['tab:green', 'tab:blue', 'tab:orange', 'tab:red']
    for v, c in zip(Vs, colors):
        ax.scatter(v.lon, v.lat, c=c, lw=.3, zorder=10,
                   edgecolors='w', s=13,
                   transform=ccrs.PlateCarree())
        for i in v.index:
            _x, _y = v.at[i, 'lon'], v.at[i, 'lat']
            xa, ya = ax.projection.transform_point(_x, _y, src_crs=ccrs.PlateCarree())
            ant = format(int(v.at[i, 'ID']), 'x')
            #ant = f'{v.at[i, "EE"]:.2f}'  # elipsis eccentricity
            #ant = f'{v.at[i, "XX"]:.2f}'  # zonal laplacian
            #ant = f'{v.at[i, "SR"]:.2f}'  # slope ratio
            ax.annotate(ant, c='k', alpha=.8,
                xy=(xa, ya), color='k', size=7, zorder=11)

    Vs = [TR, CL, RG, CH]
    colors = ['tab:green', 'tab:blue', 'tab:orange', 'tab:red']
    for v, c in zip(Vs, colors):
        ax.scatter(v.lon, v.lat, c=c, s=18, ec='w', lw=.4, zorder=10,
                   transform=ccrs.PlateCarree())
        for i in v.index:
            _x, _y = v.at[i, 'lon'], v.at[i, 'lat']
            ax.tissot(
                lons=_x, lats=_y,
                rad_km=v.at[i, 'ro'],
                facecolor='None', alpha=0.8,
                edgecolor=c, linewidth=1.6, zorder=10)

            xa, ya = ax.projection.transform_point(_x, _y, src_crs=ccrs.PlateCarree())
            ant = format(int(v.at[i, 'ID']), 'x')
            #ant = f'{v.at[i, "EE"]:.2f}'  # elipsis eccentricity
            #ant = f'{v.at[i, "XX"]:.2f}'  # zonal laplacian
            #ant = f'{v.at[i, "SR"]:.2f}'  # slope ratio
            ax.annotate(ant, c='k',
                xy=(xa, ya), color='k', size=7, zorder=11)

    ax.set_title(f'{tn} {lev} hPa')
    #plt.tight_layout()


def z_gen(z, y):

    #times = pd.to_datetime(z.time.values)
    #times = pd.date_range(f'{y}-01-01 00', f'{y}-03-01 00', freq='6H') 
    times = pd.date_range(f'{y}-01-01 00', f'{y}-01-31 18', freq='6H') 
    #times = pd.date_range(f'{y}-08-01 00', f'{y}-08-31 18', freq='6H') 
    #times = pd.date_range(f'{y}-01-01 00', f'{y}-12-31 18', freq='6H') 
    for t in times[1:]:
        yield z.sel(time=t)

lev = 300

#for y in [2014, 2015]:
for y in [2014]:
    z = xr.open_dataarray(f'./hgt{y}.nc').sel(lev=lev)

    fig = plt.figure()
    ortho = ccrs.Orthographic(central_latitude=90, central_longitude=180)
    ax = fig.add_subplot(111, projection=ortho)

    update(z.isel(time=0))

    anim = animation.FuncAnimation(fig, update, frames=z_gen(z, y), interval=1000)
    #anim.save('./fig/draw_result.mp4', writer='ffmpeg')
    #anim.save(f'./fig/colindex2_anim/{y}_7w_{lev}.mp4', writer='ffmpeg')
    #anim.save(f'./fig/colindex2_anim/{y}_4s_{lev}.mp4', writer='ffmpeg')
    #anim.save(f'./fig/colindex2_anim/{y}_8_{lev}.mp4', writer='ffmpeg')
    anim.save(f'./fig/colindex2_anim/{y}_8SR_{lev}.mp4', writer='ffmpeg')
    
    plt.close()

